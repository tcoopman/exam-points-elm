module Question.Questions where

import Html exposing (Html, text, div)
import Html.Events exposing (onClick)
import Question.Question as Question
import Debug

type Action =
  Add
  | UpdateQuestion Question.ID Question.Action

type alias Model = List Question.Model

update : Action -> Model -> Model
update action model =
  case action of
    Add ->
      let
        id = (List.length model + 1)
        question = Question.init (Debug.log "Question id: " id)
      in
        List.append model [question]
    UpdateQuestion id action ->
      let
        updateQuestion question =
          if question.id == id
            then Question.update action question
            else question
      in
        List.map updateQuestion model

view : Signal.Address Action -> Model -> Html
view address model =
  let
    action id = Signal.forwardTo address (UpdateQuestion id)
    qview question = Question.view (action question.id) question
    questions = List.map qview model
  in
    div
      []
      [ div
        [ onClick address Add ]
        [ text "Voeg vraag toe"]
      , div
        []
        questions
      , text (toString model)
      ]
