module Question.Question where

import Html exposing (..)
import Html.Events exposing (on, onClick, targetValue)

import Html.Attributes exposing (id, type', for, value, class, style)
import Css.Border as Border
import Css.Border.Style as BorderStyle
import Css.Margin as Margin
import Css.Flex as Flex
import Css.Display as Display exposing (display)
import StyleHelper
import String

type alias Tag = String

type alias ID = Int
type alias Model =
  { id: ID
  , question: String
  , score: Int
  , error: String
  , tags: List Tag
  }

init : ID -> Model
init id =
  { id = id
  , question = ""
  , score = 0
  , error = ""
  , tags = []
  }

type Action = UpdateQuestion String
  | UpdateScore String
  | UpdateTags String

update : Action -> Model -> Model
update action model =
  case action of
    UpdateQuestion str -> {model | question <- str }
    UpdateScore str -> {model | score <-
      Maybe.withDefault model.score (Result.toMaybe (String.toInt str)) }
    UpdateTags str -> {model | tags <- String.split ", " str}

view : Signal.Address Action -> Model -> Html
view address model =
  let
    questionStyle =
      []
        |> display Display.Flex
        |> Flex.direction Flex.Column
        |> Flex.justifyContent Flex.JCBetween
        |> Flex.alignItems Flex.AIStretch
        |> Border.style BorderStyle.Solid
        |> Border.width 1 1 1 1
        |> Border.radius 5 5 5 5
        |> Margin.all 10 10 10 10
    rowStyle =
      []
        |> Margin.all 10 10 10 10
        |> display Display.Flex
    labelStyle =
      []
        |> Margin.left 10
        |> Margin.right 10
        |> StyleHelper.flexBasis 120
    inputStyle =
      []
        |> Flex.grow 3
  in
    div
      [ style questionStyle ]
      [ div
          [ style rowStyle ]
          [ label [ for "question", style labelStyle ] [ text "Question:" ]
          , input
              [ type' "text"
              , value model.question
              , on "input" targetValue (\str -> Signal.message address (UpdateQuestion str))
              , style inputStyle
              ]
              []
          ]
      , div
          [ style rowStyle ]
          [ label [ for "score", style labelStyle ] [ text "Maximumscore:" ]
          , input
              [ type' "text"
              , value (toString model.score)
              , on "input" targetValue (\str -> Signal.message address (UpdateScore str))
              , style inputStyle
              ]
              []
          , label [ for "tags", style labelStyle ] [ text "tags:" ]
          , input
              [ type' "text"
              , value (String.join ", " model.tags)
              , on "input" targetValue (\str -> Signal.message address (UpdateTags str))
              , style inputStyle
              ]
              []
          ]
      ]
