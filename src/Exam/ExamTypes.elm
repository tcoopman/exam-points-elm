module Exam.ExamTypes where

import Question.Questions as Questions

type Route =
  Root
  | Edit ID
  | Create

type alias ID = Int
type alias Exam =
  { id: ID
  , name: String
  , period: String
  , schoolyear: String
  , course: String
  , classes: List String
  , questions: Questions.Model
  }

type alias Model = List Exam

init : Model
init = []
