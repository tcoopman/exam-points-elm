module Exam.Page where

import Exam.ExamTypes exposing (..)
import Html exposing (Html, div, h1, text, label, input)
import Html.Attributes exposing (type', for, value)
import Html.Events exposing (onClick, on, targetValue)
import Layout.MainSplit as MainSplitLayout
import Question.Questions as Questions

type Action =
  New
  | UpdateName ID String
  | UpdateQuestions ID Questions.Action

newExam : ID -> Exam
newExam id =
  { id = id
  , name = ""
  , period = ""
  , schoolyear = ""
  , course = ""
  , classes = []
  , questions = []
  }

update : Action -> Model -> (Model, Route)
update action model =
  case action of
    New ->
      let
        newId = List.length model + 1
        exam = newExam newId
      in
        (exam :: model, Edit newId)
    UpdateName id str ->
      let
        updateName exam =
          case exam.id of
            id -> {exam | name <- str}
            _ -> exam
        newModel = List.map updateName model
      in
        (newModel, Edit id)
    UpdateQuestions id action' ->
      let
        updateQuestions exam =
          if exam.id == id
            then { exam | questions <- (Questions.update action' exam.questions )}
            else exam
      in
        (List.map updateQuestions model, Edit id)

view : Signal.Address Action -> Model -> Route -> Html
view address model route =
  let controls = viewControl address model route
  in
    case route of
      Root ->
        viewRoot address model
      Create ->
        let main = viewEdit address (newExam 0)
        in
          MainSplitLayout.view (MainSplitLayout.init controls main)
      Edit id ->
        let
          maybeExam = List.head (List.filter (\i -> i.id == id) model)
          exam = Maybe.withDefault (newExam 0) maybeExam
          main = viewEdit address exam
        in
          MainSplitLayout.view (MainSplitLayout.init controls main)
      _ ->
        let
          main = text "main"
        in
          MainSplitLayout.view (MainSplitLayout.init controls main)

viewRoot : Signal.Address Action -> Model -> Html
viewRoot address model =
  div
    []
    [ h1 [] [ text "Examens" ]
    , div
      [ onClick address New]
      [ text "Nieuw examen" ]
    ]

viewEdit : Signal.Address Action -> Exam -> Html
viewEdit address exam =
  div
    []
    [ h1 [] [ text "Bewerk examen" ]
    , div
      []
      [ label [ for "course" ] [ text "Vak: "]
      , input
        [ type' "text"
        , value exam.course
        ]
        []
      ]
    , div
      []
      [ label [ for "classes" ] [ text "Klassen: "]
      , input
        [ type' "text"
        , value (toString exam.classes)
        ]
        []
      ]
    -- , div
    --   []
    --   [ label [ for "name" ] [ text "Naam:" ]
    --   , input
    --     [ type' "text"
    --     , value exam.name
    --     , on "input" targetValue (\str -> Signal.message address (UpdateName exam.id str))
    --     ]
    --     []
    --   ]
    , div
      []
      [ label [ for "period" ] [ text "Periode: " ]
      , input
        [ type' "text"
        , value exam.period
        ]
        []
      ]
    , div
      []
      [ label [ for "schoolyear" ] [ text "Schooljaar: "]
      , input
        [ type' "text"
        , value exam.schoolyear
        ]
        []
      ]
    , Questions.view (Signal.forwardTo address (UpdateQuestions exam.id)) exam.questions
    ]

viewControl : Signal.Address Action -> Model -> Route -> Html
viewControl address model route =
  let
    routeName =
      case route of
        Root -> "Root"
        Edit id -> "Bewerk"
        Create -> "Nieuw"
  in
    div
      []
      [ h1 [] [ text "Examens" ]
      , routeName |> text
      , div
        []
        (List.map (.name >> text) model)
      ]
