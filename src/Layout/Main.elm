module Layout.Main where

import Color exposing (rgba, white)
import Html exposing (Html, div, text, pre, code, ul, li)
import Html.Attributes as Attributes exposing (style)
import Css exposing (Styles, px)
import Css.Flex as Flex
import Css.Background as Background
import Css.Dimension as Dimension
import Css.Border.Top as BorderTop
import Css.Border.Style as BorderStyle
import Css.Display as Display exposing (display)
import StyleHelper

layout : Styles -> Styles
layout styles =
  styles
    |> display Display.Flex
    |> Flex.alignItems Flex.AIStretch
    |> Dimension.height 600
    |> Flex.grow 1

column : Styles -> Styles
column styles =
  styles
    |> display Display.Flex
    |> StyleHelper.flexBasis 200

mainLayout : Styles -> Styles
mainLayout styles =
  styles
    |> display Display.Flex
    |> Flex.direction Flex.Column
    |> Flex.grow 1

borderTop : Color.Color -> Styles -> Styles
borderTop color styles =
  styles
    |> BorderTop.color color
    |> BorderTop.style BorderStyle.Solid
    |> BorderTop.width 4

view : Html -> Html
view html =
  div
    [ style
      <| layout
      <| borderTop (rgba 0 119 165 1)
      <| Background.color (rgba 247 248 248 1)
      <| []
    ]
    [ html ]
