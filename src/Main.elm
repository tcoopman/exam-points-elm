import Html exposing (Html, div, text)
import MainTypes exposing (..)
import UI

actions : Signal.Mailbox UI.Action
actions =
  Signal.mailbox UI.NoOp

model : Signal State
model =
  Signal.foldp UI.update init actions.signal

main : Signal Html
main =
  Signal.map (view actions.address) model

view : Signal.Address UI.Action -> State -> Html
view = UI.view

-- port saveActions : Signal.Mailbox UI.Action
-- port saveActions = actions

-- type alias Saver =
--   { nav: NavColumn.Model
--   , page: String
--   , exams: ExamsPage.Model
--   }
--
-- filterF : UI.Model -> Saver -> Saver
-- filterF model toSave = { page = model.page, nav = model.nav, exams = model.exams}
--
-- filterModel : Signal Saver
-- filterModel =
--   Signal.foldp filterF { nav = NavColumn.init, page = "", exams = [] } model
--
-- port saveSomething : Signal Saver
-- port saveSomething = filterModel

-- port setStorage : Signal UI.Model
-- port setStorage = model
