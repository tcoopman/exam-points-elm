module Nav.NavTypes where

type alias ModelItem =
  { name: String
  , active: Bool
  }
type alias Model = List ModelItem

init : String -> Bool -> Model -> Model
init name active model =
  model ++ [{name = name, active = active}]
