module Nav.Nav where

import Nav.Button as Button
import Nav.NavTypes exposing (..)
import Html exposing (Html, div, button, text, pre, code, ul, li)
import Html.Attributes as Attributes exposing (style)
import Css.Flex as Flex
import Css.Display as Display exposing (display)
import Debug
import MainTypes

update : Action -> Model -> Model
update action model =
  case action of
    SetPage name ->
      let
        updateActive model =
          if model.name == name
            then { model | active <- True}
            else { model | active <- False}
      in
        List.map updateActive model

view : Signal.Address Action -> Model -> Html
view address model =
  let items = List.map (viewButton address) model
  in
    div
      [ style
          <| display Display.Flex
          <| Flex.direction Flex.Column
          <| Flex.grow 1
          <| []
      ]
      items

type Action =
  SetPage String

viewButton : Signal.Address Action -> ModelItem -> Html
viewButton address model =
  let
    buttonModel = Button.init model.name model.active
  in
    Button.view (Signal.forwardTo address (always (SetPage model.name))) buttonModel
