module Nav.Button (Model, init, view) where

import Color exposing (rgba, white)
import Html exposing (Html, div, button, text, pre, code, ul, li)
import Html.Attributes as Attributes exposing (style)
import Html.Events exposing (onClick)
import Css exposing (Styles, px)
import Css.Flex as Flex
import Css.Dimension as Dimension
import Css.Text as Text
import Css.Border.Left as BorderLeft
import Css.Border.Bottom as BorderBottom
import Css.Border.Style as BorderStyle
import Css.Display as Display exposing (display)
import Css.Padding as Padding

type alias Model =
  { text: String
  , active: Bool
  }

init : String -> Bool -> Model
init text active =
  { text = text
  , active = active
  }

view : Signal.Address () -> Model -> Html
view address model =
  div
    [ style (itemStyle model.active [])
    , onClick address ()
    ]
    [ text model.text ]


itemStyle : Bool -> Styles -> Styles
itemStyle active styles =
  let basic =
    styles
      |> Text.color (rgba 148 160 165 1)
      |> Dimension.height 50
      |> BorderBottom.color (rgba 68 78 82 1)
      |> BorderBottom.style BorderStyle.Solid
      |> BorderBottom.width 1
      |> display Display.Flex
      |> Flex.direction Flex.Column
      |> Flex.justifyContent Flex.JCCenter
      |> Padding.left 15
  in
    case active of
      True ->
        basic
          |> Text.color (rgba 255 255 255 1)
          |> BorderLeft.color (rgba 34 183 237 1)
          |> BorderLeft.width 5
          |> Padding.left 10
          |> BorderLeft.style BorderStyle.Solid
      False ->
        basic
