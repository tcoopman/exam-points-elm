module MainTypes where

import Exam.ExamTypes as ExamTypes
import Config.ConfigTypes as ConfigTypes
import Nav.NavTypes as NavTypes

type alias State =
  { model : Model
  , route : Route
  }

buildState : Model -> Route -> State
buildState model route =
  { model = model
  , route = route
  }

type alias Model =
  { nav: NavTypes.Model
  , exam: ExamTypes.Model
  , config: ConfigTypes.Model
  }

examens = "Examens"
configuratie = "Configuratie"

type Route =
  Exam ExamTypes.Route
  | Config ConfigTypes.Route

init : State
init =
  { model = initModel
  , route = initRoute
  }

initModel : Model
initModel =
  { nav = []
      |> NavTypes.init examens True
      |> NavTypes.init configuratie False
  , exam = ExamTypes.init
  , config = ConfigTypes.init
  }

initRoute : Route
initRoute = Exam ExamTypes.Root

routeToString : Route -> String
routeToString route =
  case route of
    Exam _ -> examens
    Config _ -> configuratie
