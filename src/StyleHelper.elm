module StyleHelper where

import Css exposing (Styles, px)

flexBasis : number -> Styles -> Styles
flexBasis b styles =
  Css.style "flex-basis" (px b) styles
