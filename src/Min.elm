type alias State =
  { model : Model
  , route : Route
  }

buildState : Model -> Route -> State
buildState model route =
  { model = model
  , route = route
  }

type alias Model =
  { nav: String
  , exam: String
  , config: String
  }

type Route =
  Exam
  | Config

type Action =
  NoOp
  | SetPage
  | Page

update : Action -> State -> State
update action state =
  case action of
    NoOp -> state
    SetPage ->
      let
        model = state.model
        activeName m =
          Maybe.withDefault {name = "", active = False} (List.head (List.filter .active m))
        nav = model.nav
      in
        { model | nav <- nav
        , page <- .name (activeName nav)
        }
    Page ->
      let
        model = state.model
        -- (exams, route) = ExamPage.update action model.exams
      in
        buildState model state.route
