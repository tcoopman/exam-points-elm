module UI where

import Html exposing (Html, text)
import Nav.Nav as NavColumn
import Exam.Page as ExamPage
import Exam.ExamTypes as ExamTypes
import MainTypes as MainTypes exposing (State, buildState)
import Layout.NavMain as NavMainLayout

type Action =
  NoOp
  | SetPage NavColumn.Action
  | Page ExamPage.Action

update : Action -> State -> State
update action state =
  case action of
    NoOp -> state
    SetPage action ->
      let
        model = state.model
        activeName m =
          Maybe.withDefault {name = "", active = False} (List.head (List.filter .active m))
        nav = NavColumn.update action model.nav
        -- FIXME update the route!!!!
        route = state.route
      in
        buildState { model | nav <- nav } route
    Page action ->
      let
        model = state.model
        (exam, route) = ExamPage.update action model.exam
      in
        buildState { model | exam <- exam} (MainTypes.Exam (ExamTypes.Edit 1))

view : Signal.Address Action -> State -> Html
view address state =
  let
    model = state.model
    first = NavColumn.view (Signal.forwardTo address (SetPage)) model.nav
    main = viewPage address state
  in
    NavMainLayout.view (NavMainLayout.init first main)

viewPage : Signal.Address Action -> State -> Html
viewPage address state =
  case state.route of
    MainTypes.Exam route -> ExamPage.view (Signal.forwardTo address (Page)) state.model.exam route
    _ -> text "None"
